## Criando chaves GPG Ed25519 com subchaves:


-- Crie sua chave com o comando gpg junto com o parametro "--expert" para ter mais opções durante a \
criação.


```bash
user@hostname:~$ gpg --full-gen-key --expert
gpg (GnuPG) 2.2.27; Copyright (C) 2021 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Por favor selecione o tipo de chave desejado:
   (1) RSA and RSA (default)
   (2) DSA and Elgamal
   (3) DSA (apenas assinatura)
   (4) RSA (apenas assinatura)
   (7) DSA (set your own capabilities)
   (8) RSA (set your own capabilities)
   (9) ECC and ECC
  (10) ECC (sign only)
  (11) ECC (set your own capabilities)
  (13) Existing key
  (14) Existing key from card
Opção? 11
```
-- Selecione a opção 11.
```bash
Possible actions for a ECDSA/EdDSA key: Sign Certify Authenticate 
Current allowed actions: Sign Certify 

   (S) Toggle the sign capability
   (A) Toggle the authenticate capability
   (Q) Finished

Opção? s
```
-- Selecione a opção "s" para que tenhamos apenas o certificado/chave primaria nesse momento.
```bash
Possible actions for a ECDSA/EdDSA key: Sign Certify Authenticate 
Current allowed actions: Certify 

   (S) Toggle the sign capability
   (A) Toggle the authenticate capability
   (Q) Finished

Opção? q
```
-- Selecione "q" para finalizar as escolhas.
```bash
Please select which elliptic curve you want:
   (1) Curve 25519
   (3) NIST P-256
   (4) NIST P-384
   (5) NIST P-521
   (6) Brainpool P-256
   (7) Brainpool P-384
   (8) Brainpool P-512
   (9) secp256k1
Opção? 1
```
-- Digite "1" para escolher a curva elíptica Ed25519.
```bash
Por favor especifique por quanto tempo a chave deve ser válida.
         0 = chave não expira
      <n>  = chave expira em n dias
      <n>w = chave expira em n semanas
      <n>m = chave expira em n meses
      <n>y = chave expira em n anos
A chave é valida por? (0) 50y
```
-- Digite o tempo que o certificado levará para expirar. Utilize um tempo da sua expectativa de vida, \
seguido de uma das letras (ou não) para definir dias, meses, semanas ou anos.
```bash
Key expires at seg 01 fev 2072 05:43:28 -03
Is this correct? (y/N) y
```
-- Confirme a sua escolha.
```bash
GnuPG needs to construct a user ID to identify your key.

Nome completo: Yuri Musachio Montezuma da Cruz
```
-- Digite seu nome completo.
```bash
Endereço de correio eletrónico: yuri.musachio@gmail.com
```
-- Endereço de email.
```bash
Comentário:
```
-- Se houver.
```bash
Você selecionou este identificador de utilizador:
    "Yuri Musachio Montezuma da Cruz <yuri.musachio@gmail.com>"

Mudar (N)ome, (C)omentário, (E)ndereço ou (O)k/(S)air? o
```
-- Se estiver tudo correto, confirme com a letra "o". Será aberta uma janela de interação para a \
criação da senha.
```bash
Precisamos gerar muitos bytes aleatórios. É uma boa ideia realizar outra
actividade (escrever no teclado, mover o rato, usar os discos) durante a
geração dos números primos; isso dá ao gerador de números aleatórios
uma hipótese maior de ganhar entropia suficiente.
gpg: /home/ymusachio/.gnupg/trustdb.gpg: base de dados de confiança criada
gpg: key 0A25CAF2ECDCE423 marked as ultimately trusted
gpg: directory '/home/ymusachio/.gnupg/openpgp-revocs.d' created
agpg: revocation certificate stored as '/home/ymusachio/.gnupg/openpgp-revocs.d/1BF4573094309A91703443C30A25CAF2ECDCE423.rev'
chaves pública e privada criadas e assinadas.

pub   ed25519 2022-02-15 [C] [expires: 2072-02-03]
      1BF4573094309A91703443C30A25CAF2ECDCE423		<------- ID chave primária
uid                      Yuri Musachio Montezuma da Cruz <yuri.musachio@gmail.com>
```
-- Após isso, seu certificado/chave primária será criada. O próximo passo será criar as subchaves, \
e para isso será necessário editar o certificado/chave primária que acabamos de criar.
```bash
user@hostname:~$ gpg --expert --edit-key id-chave-primaria
gpg (GnuPG) 2.2.27; Copyright (C) 2021 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Chave secreta disponível.

gpg: a verificar a base de dados de confiança
gpg: marginals needed: 3  completes needed: 1  trust model: pgp
gpg: depth: 0  valid:   1  signed:   0  trust: 0-, 0q, 0n, 0m, 0f, 1u
gpg: proxima verificação da base de dados de confiança a 2072-02-03
sec  ed25519/0A25CAF2ECDCE423
     created: 2022-02-15  expires: 2072-02-03  usage: C   
     trust: ultimate      validity: ultimate
[ultimate] (1). Yuri Musachio Montezuma da Cruz <yuri.musachio@gmail.com>

gpg> addkey
```
-- Digite "addkey" para adicionar uma subchave, vinculada a chave primária.
```bash
Por favor selecione o tipo de chave desejado:
   (3) DSA (apenas assinatura)
   (4) RSA (apenas assinatura)
   (5) Elgamal (encrypt only)
   (6) RSA (apenas cifragem)
   (7) DSA (set your own capabilities)
   (8) RSA (set your own capabilities)
  (10) ECC (sign only)
  (11) ECC (set your own capabilities)
  (12) ECC (encrypt only)
  (13) Existing key
  (14) Existing key from card
Opção? 11
```
-- Selecione a opção "11".
```bash
Possible actions for a ECDSA/EdDSA key: Sign Authenticate 
Current allowed actions: Sign 

   (S) Toggle the sign capability
   (A) Toggle the authenticate capability
   (Q) Finished

Opção? q
```
-- Digite "q" para deixarmos o tipo da chave como "Sign".
```bash
Please select which elliptic curve you want:
   (1) Curve 25519
   (3) NIST P-256
   (4) NIST P-384
   (5) NIST P-521
   (6) Brainpool P-256
   (7) Brainpool P-384
   (8) Brainpool P-512
   (9) secp256k1
Opção? 1
```
-- Digite "1" para escolher a curva elíptica Ed25519.
```bash
Por favor especifique por quanto tempo a chave deve ser válida.
         0 = chave não expira
      <n>  = chave expira em n dias
      <n>w = chave expira em n semanas
      <n>m = chave expira em n meses
      <n>y = chave expira em n anos
A chave é valida por? (0) 3y
```
-- Digite o tempo em que a chave irá expirar. Por se tratar de subchaves, elas podem ter um período \
menor que o certificado/chave primária.
```bash
Key expires at sex 14 fev 2025 00:38:41 -03
Is this correct? (y/N) y
```
-- Confirme as informações.
```bash
Really create? (y/N) y
```
-- Confirme a criação. Após isso, será aberta uma janela interativa pedindo para que você digite \
a senha da sua chave primária, pois sua subchave será criada vinculada ao certificado master \
utilizando a mesma senha. (Poderemos modificar a senha que será utilizada na subchave futuramente, \
se assim desejarmos)
```bash
Precisamos gerar muitos bytes aleatórios. É uma boa ideia realizar outra
actividade (escrever no teclado, mover o rato, usar os discos) durante a
geração dos números primos; isso dá ao gerador de números aleatórios
uma hipótese maior de ganhar entropia suficiente.

sec  ed25519/0A25CAF2ECDCE423
     created: 2022-02-15  expires: 2072-02-03  usage: C   
     trust: ultimate      validity: ultimate
ssb  ed25519/84B3FA13F3516D70
     created: 2022-02-15  expires: 2025-02-14  usage: S   
[ultimate] (1). Yuri Musachio Montezuma da Cruz <yuri.musachio@gmail.com>
```
-- Subchave criada. Perceba que ao lado de "usage" é mostrado uma sigla que representa a finalidade \
daquela chave. "C" significa Certify/Certificado (Certificado/chave master), "S" de Sign/Assinatura \
(Utilizado para assinar coisas), "E" de Encrypt/Encriptação (Utilizado para encriptar algo), e "A" de \
Authenticate/Autenticação (Para se autenticar em algo). Dependendo das opções que selecionarmos \
durante a criação das subchaves, podem haver tipos de usos em conjunto como "SA", utilizado para \
assinar e autenticar. Ao final, digite "save" para salvar todas as modificações feitas.
```bash
gpg> save
```
