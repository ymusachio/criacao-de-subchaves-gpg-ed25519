## Modificação de senhas e exportação de chaves

### Modificando senha.
-- Após apagar a keygrip da chave primária, iremos modificar as senhas das subchaves criadas. Liste o \
ID das subchaves criadas.
```bash
user@hostname:~$ gpg --list-keys --with-subkey-fingerprint
/home/ymusachio/.gnupg/pubring.kbx
----------------------------------
pub   ed25519 2022-02-15 [C] [expires: 2072-02-03]
      1BF4573094309A91703443C30A25CAF2ECDCE423		<------- ID chave primária
uid           [ultimate] Yuri Musachio Montezuma da Cruz <yuri.musachio@gmail.com>
sub   ed25519 2022-02-15 [S] [expires: 2025-02-14]
      5B52DD1C1B23FEC7CE105D7B84B3FA13F3516D70		<------- ID subchave Sign
```
-- Iremos modificar apenas a senha das subchaves, para que a senha master não fique sendo digitada \
constantemente.
```bash
user@hostname:~$ gpg --expert --edit-key id-subchave
gpg (GnuPG) 2.2.27; Copyright (C) 2021 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Secret subkeys are available.

pub  ed25519/0A25CAF2ECDCE423
     created: 2022-02-15  expires: 2072-02-03  usage: C   
     trust: ultimate      validity: ultimate
ssb  ed25519/84B3FA13F3516D70
     created: 2022-02-15  expires: 2025-02-14  usage: S   
[ultimate] (1). Yuri Musachio Montezuma da Cruz <yuri.musachio@gmail.com>
```
-- Digite "password" para modificar a senha da subchave. Será solicitado primeiramente uma senha (igual \
a senha da chave primária), e depois digite uma nova senha. \
**Atenção:** Há um limite de tempo para a \
digitação das senhas.
```bash
gpg> password
```
-- Será apresentada uma mensagem parecida com essa... Pode ignora-la, pois não é um erro referente a \
modificação da senha da nossa subchave.
```bash
gpg: key 0A25CAF2ECDCE423/0A25CAF2ECDCE423: error changing passphrase: Sem chave secreta

```
-- Salve as alterações.
```bash
gpg> save
```
### Exportando subchaves
-- Agora vamos exportar a subchave que iremos utilizar no projeto Debian.
```bash
user@hostname:~$ gpg --list-secret-keys --with-subkey-fingerprint
/home/ymusachio/.gnupg/pubring.kbx
----------------------------------
sec#  ed25519 2022-02-15 [C] [expires: 2072-02-03]
      1BF4573094309A91703443C30A25CAF2ECDCE423
uid           [ultimate] Yuri Musachio Montezuma da Cruz <yuri.musachio@gmail.com>
ssb   ed25519 2022-02-15 [S] [expires: 2025-02-14]
      5B52DD1C1B23FEC7CE105D7B84B3FA13F3516D70		<------- ID subchave Sign
```
-- Informe o ID da subchave com o caracter "!". O caracter "!" após o ID da subchave informa que você quer exportar apenas aquela subchave em específico, caso possua mais de uma subchave.
```bash
user@hostname:~$ gpg -a --export-secret-subkeys 5B52DD1C1B23FEC7CE105D7B84B3FA13F3516D70! > /destino/salva/subkey.gpg
```
-- Pronto! Agora só importar a sua chave para a Jaula e continuar com o fluxo de configuração da chave \
escolhida.
