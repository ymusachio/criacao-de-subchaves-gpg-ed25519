## Criando Backup das chaves criadas:

-- Crie um backup de sua chave GPG, e armazene em algum lugar seguro. O intuito deste backup é manter \
a sua chave GPG primária fora do seu computador, evitando que ela seja roubada. Aconselho a criar um \
pendrive criptografado com LUKS habilitado. \
**ATENÇÂO**: Após criar o pendrive criptografado, e acessa-lo pela primeira vez, lembre de nunca \
salvar a senha de acesso do pendrive no seu Sistema.
```bash
user@hostname:~$ tar -cvf gnupg-backup.tar .gnupg/
```
-- Transfira-o para seu device criptografado.
```bash
user@hostname:~$ cp gnupg-backup.tar /caminho/device/encriptado/
```
-- Após a transferência do backup, verifique a keygrip da chave primária e exclua-a.
```bash
user@hostname:~$ gpg --list-key --with-keygrip
/home/ymusachio/.gnupg/pubring.kbx
----------------------------------
pub   ed25519 2022-02-15 [C] [expires: 2072-02-03]
      1BF4573094309A91703443C30A25CAF2ECDCE423
      Keygrip = 49A1A1035096D8F9835B30047D9D37E51BD5C214	<------- Keygrip chave primária
uid           [ultimate] Yuri Musachio Montezuma da Cruz <yuri.musachio@gmail.com>
sub   ed25519 2022-02-15 [S] [expires: 2025-02-14]
      Keygrip = 8E7BE8F12D37A5E8C1D830E4C5F06D7FF1B034BD
```
**Atenção:** Remova a keygrip da chave primária apenas após o backup das chaves.
```bash
user@hostname:~$ rm .gnupg/private-keys-v1.d/keygrip-primaria.key
```
**Atenção:** A exclusão da keygrip da chave primária implica na impossibilidade de modificar ou \
adicionar novas subchaves... Por isso é muito importante efetuar o backup das chaves.
