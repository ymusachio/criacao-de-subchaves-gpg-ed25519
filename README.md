# Chave GPG

## Os documentos no projeto exemplificam a maneira de se criar chaves GPG com subchaves, utilizando a curva elíptica Ed25519.

O foco desses documentos são para a criação de chaves que possam ser utilizadas no projeto Debian, para os que tem a vontade de contribuir com o projeto. Claro que certas configurações podem ser modificadas para se adequarem as suas diferentes necessidades.

1. [Criando chaves GPG Ed25519 com subchaves](https://salsa.debian.org/ymusachio/criacao-de-subchaves-gpg-ed25519/-/blob/master/Subchaves.md)
2. [Criando Backup das chaves GPG](https://salsa.debian.org/ymusachio/criacao-de-subchaves-gpg-ed25519/-/blob/master/Backup.md)
3. [Modificação de senhas e exportação de chaves](https://salsa.debian.org/ymusachio/criacao-de-subchaves-gpg-ed25519/-/blob/master/Password-export.md)






















Agradecimentos: Paulo Roberto Alves de Oliveira (aka kretcheu) / Luis Leal

Fontes: \
        [Generate GPG Keys With Curve Ed25519](https://www.digitalneanderthal.com/post/gpg/) \
        [Usando subchaves OpenPGP no desenvolvimento do Debian](https://wiki.debian.org/pt_BR/Subkeys) \
        [GnuPG](https://wiki.archlinux.org/title/GnuPG)
